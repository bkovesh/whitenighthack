// pages
import PageMain from './pages/PageMain'
// import PageUsers from './pages/PageUsers'
// import PagePromises from './pages/PagePromises'
// import PageUser from './pages/PageUser'
import PageCabinet from './pages/PageCabinet'
import Page404 from './pages/Page404'


// components
import MenuMain from './containers/MenuMain'
import ModalPay from './containers/ModalPay'
import ModalForAll from './containers/ModalForAll'
import MapMain from './containers/MapMain'
import MapPick from './containers/MapPick'
import MapShow from './containers/MapShow'


export {
  // pages
  PageMain,
  // PageUsers,
  // PagePromises,
  // PageUser,
  PageCabinet,
  Page404,
  // components
  MenuMain,
  ModalPay,
  ModalForAll,
  MapMain,
  MapPick,
  MapShow,
}
